<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js wf-bree-n4-active wf-bree-n7-active wf-breeserif-n7-active wf-breeserif-n3-active wf-active"><!--<![endif]-->

<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Coder Forge - Home</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/icon.png" />

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style type="text/css">.tk-bree{font-family:"bree",sans-serif;}.tk-bree-serif{font-family:"bree-serif",serif;}</style><style type="text/css">@font-face{font-family:bree;src:url(https://use.typekit.net/af/be7132/000000000000000000013389/27/l?subset_id=2&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/be7132/000000000000000000013389/27/d?subset_id=2&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/be7132/000000000000000000013389/27/a?subset_id=2&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;}@font-face{font-family:bree;src:url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/l?subset_id=2&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/d?subset_id=2&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/a?subset_id=2&fvd=n7&v=3) format("opentype");font-weight:700;font-style:normal;}@font-face{font-family:bree-serif;src:url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/l?subset_id=2&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/d?subset_id=2&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/a?subset_id=2&fvd=n7&v=3) format("opentype");font-weight:700;font-style:normal;}@font-face{font-family:bree-serif;src:url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/l?subset_id=2&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/d?subset_id=2&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/a?subset_id=2&fvd=n3&v=3) format("opentype");font-weight:300;font-style:normal;}</style><script type="text/javascript">try{Typekit.load({async: false});}catch(e){}</script>

    <link rel="stylesheet" href="dist/css/9b21f50b6868.css" type="text/css">
    <link rel="stylesheet" href="dist/css/main.css" type="text/css">

</head>

<body>

    <section class="container">
        <header class="row" id="header">
            <!-- Begin grid -->
            <a href="https://coder-forge.com">
                <h1><img src="img/profile.jpg"
                     class="logo img-rounded" height="42" width="42"/>
                <span id="pyhead">Coder Forge</span></h1></a>
            <!-- End grid -->
        </header>
        <!-- header -->
    </section> <!-- container -->

    <div class="menu">
        <?php include 'menu.php';?>
    </div>

<div class="container">
    <div class="row well">
        <div class="col-md-8">
            <div class="row">
                <article>
                
                    <h2>Introduction:</h2>
                        
                        <section class="block-paragraph">
                            <div class="rich-text">
                            <p>
                                Coder Forge is a coding meetup to Learn to code for free every Tuesday in Temple Bar, Dublin.
                                It's for people who want to learn how to code from people who know how to code!
                            </p>
                            </div>
                        </section>

                    <h3>Where Do We Meet?</h3>

                    <section class="block-paragraph">
                        <div class="rich-text">
                        <p>
                            We meet every Tuesdays from (19h to 21h) or (7pm - 9pm) at the Brick Alley Cafe, 25 Essex St E, Temple Bar, Dublin, D02 W560! 
                        </p>
                        <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2381.880054839293!2d-6.266677584269861!3d53.34540327997877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48670fe44ae2b6e9%3A0xd679b49e440fc45d!2sBrick+Alley+Cafe!5e0!3m2!1sen!2sie!4v1565120761707!5m2!1sen!2sie"
                        width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </section>

                    <h3>How Is It Done?</h3>

                    <section class="block-paragraph">
                        <div class="rich-text">
                        <p>
                            Coder Forge adapts programming learning to your interest. Want to know how to create websites using HTML and CSS, while adding some interactivity with Javascript? Heard about Java and want to know what it is about? A friend said that python is a beginner-friendly programming language and want to give it a try? Maybe a project is ideal for you, or maybe you might prefer an indication for a good book or video lessons on the subject. With Coder Forge our members share their skills and programming know how to make you learn how to code in a way that works for you.  
                        </p>
                        </div>
                    </section>

                    <h3>What we do.</h3>

                    <section class="block-paragraph">
                            <div class="rich-text">
                            <p>
                                    Knowing how to code is an extremely useful skill in the technology driven world that we live today. Learning how to code, on the other hand, can be a very intimidating challenge.
                            </p>
                            <p>
                                    With that in mind, the Coder Forge borrows the idea of the "Coder Dojo" and applies it to adults. The objective is to create a environment where experienced software developers teach people who are interested in learning how to code! 
                            </p>

                            </div>
                        </section>
    
                    <h3>Want To Know More About Us?</h3>
                            
                     <li class="list-group-item">
                                                                               
                                        <a href="https://www.meetup.com/Dublin-Coder-Forge/">
                                            <img src="img/meetupicon.png" id="forgeLogo" height="64" width="64"> 
                                        </a>
                                    
                                        <a href="https://www.facebook.com/coderforge">
                                            <img src="img/fbicon.png" id="forgeLogo" height="64" width="64">
                                        </a>
                                <a href="https://join.slack.com/t/coderforgeworkspace/shared_invite/enQtODE3NTY5NTUxMTkxLWY2NmJkODYxYWMxMTFhYmUyMmQ3ZDhiODE3ZWNmMzU5MDUxMzM4OGRlZThkM2U0Zjc5NDcyYzU2NDVhNTNmNzQ">
                                    <img src="img/slack.png" id="forgeLogo" height="64" width="64">
                                </a>
                    </li>
                    
                </article>
            </div>
        </div>
    </div>
</div>

    <script src="dist/js/jquery-3.4.1.min.js"></script>                    
    <script src="dist/js/umd/popper.min.js"></script>                   
    <script src="dist/js/bootstrap.min.js"></script>
    
</body>
</html>