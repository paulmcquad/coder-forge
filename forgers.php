<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js wf-bree-n4-active wf-bree-n7-active wf-breeserif-n7-active wf-breeserif-n3-active wf-active"><!--<![endif]-->

<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Coder Forge - Home</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/icon.png" />

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style type="text/css">.tk-bree{font-family:"bree",sans-serif;}.tk-bree-serif{font-family:"bree-serif",serif;}</style><style type="text/css">@font-face{font-family:bree;src:url(https://use.typekit.net/af/be7132/000000000000000000013389/27/l?subset_id=2&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/be7132/000000000000000000013389/27/d?subset_id=2&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/be7132/000000000000000000013389/27/a?subset_id=2&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;}@font-face{font-family:bree;src:url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/l?subset_id=2&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/d?subset_id=2&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/a?subset_id=2&fvd=n7&v=3) format("opentype");font-weight:700;font-style:normal;}@font-face{font-family:bree-serif;src:url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/l?subset_id=2&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/d?subset_id=2&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/a?subset_id=2&fvd=n7&v=3) format("opentype");font-weight:700;font-style:normal;}@font-face{font-family:bree-serif;src:url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/l?subset_id=2&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/d?subset_id=2&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/a?subset_id=2&fvd=n3&v=3) format("opentype");font-weight:300;font-style:normal;}</style><script type="text/javascript">try{Typekit.load({async: false});}catch(e){}</script>

    <link rel="stylesheet" href="dist/css/9b21f50b6868.css" type="text/css">
    <link rel="stylesheet" href="dist/css/main.css" type="text/css">

</head>

<body class="template-home-page" cz-shortcut-listen="true">

    <section class="container">
        <header class="row" id="header">
            <!-- Begin grid -->
            <a href="https://coder-forge.com">
                <h1><img src="img/profile.jpg"
                     class="logo img-rounded" height="42" width="42"/>
                <span id="pyhead">Coder Forge</span></h1></a>
            <!-- End grid -->
        </header>
        <!-- header -->
    </section> <!-- container -->

    <div class="menu">
        <?php include 'menu.php';?>
    </div>

<div class="container">
    <div class="row well">
        <div class="col-md-8">
            <div class="row">
                <article>
        
                    <section id="forgers">

                        <h2>Forgers:</h2>

                        <h3>Daithi Coombes</h3>

                        <p>
                         founder of Coder Forge. He has 12 years experience as a digital nomad and 6 years working in
                         house. Starting as a full stack engineer he has moved to Distributed Ledger Technologies. Currently he is a
                         Smart
                         Contract Advisor for Gaimin and also working full time as a Blockchain Engineer with Blockdaemon.
                        </p>
                        <p id="caption"> code is revolution ðŸ´</p>


                        <h3>Paul McQuade</h3>
                        <img src="img/people/Paul.jpg" style="float: left; margin-right: 10px; margin-bottom: 5px;" ></img>

                        <p> The current organizer of coder forge and author of this website </p>

                        <p>
                         Started of as a student in Institute of Technology Tallaght in 2006. Got my Higher Cert in
                         Engineering in 2008. Joined coder-forge meetup in January of 2019. Active user on Github: <a
                         href="https://www.github.com/paulmcquad/">www.github.com/paulmcquad</a>. Has over 65 repositories and loves
                         open source. Loves web development languages like HTML5, CSS3 and Javascript. Check out my <a
                         href="https://github.com/paulmcquad/CV">CV</a> for more information. Coding is my hooby but I think I would
                         like to get a job in 2020.
                        </p>                    

                        <br/>
                        <br/>
                        <br/>
       
                        <h3>Alan Dodd</h3>
                        <img src="img/people/Alan.jpg" width="200" height="200" style="float: left; margin-right: 10px; margin-bottom: 5px;" ></img>
                        
                        <p>
                          Hobbyist coder who runs a number of affiliate websites including cheaptextbooks dot com and
                          allhotels dot com. Rudimentary basic knowledge of HTML, CSS and PHP. Here to learn!
                        </p>

                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>

                        <h3>Caue Duarte</h3>
                        <img src="img/people/Caue.jpg" width="200" height="200" style="float: left; margin-right: 10px; margin-bottom: 5px;" ></img>


                        <p>
                          3rd year student pursuing a Bsc in Information Technology. Started going to the Coder Forge to
                          speak to people that work with tech to get ideas on things to learn in this last year in college. Has a
                          general
                          knowledge about programming and other IT fields, having Java as his strong suit and some web development
                          knowledge (HTML, CSS, PHP). Here to learn and help other people when possible!
                        </p>

                        <br/>
                        <br/>
                        <br/>

                        <h3>Agnieszka Kowalczyk (Agi)</h3>
                        <p>
                          Psychology PhD graduate with background in health/social care; asipring Data Scientist.
                          Competent in using R-programing language (e.g., cleaning, preparing, analysing and visualising quantitative
                          data). Joined the Code Forger to learn and practice programing languages (e.g., Python, Java).
                        </p>

                        <h3>Daniel Onyedikachi Oboko</h3>

                        <p>
                          MSc in Applied Digital Media with a background in Computer Networking Engineering;
                          currently a Web Developer, managing an e-commerce website. Working with(PHP,Javascript,HTML/CSS). Digital Story Telling, Photographer, writer. working towards becoming a Software Developer. Joined the Coder Forger to learn and practice programing and all round Tech.
                        </p>

                    </section>




                </article>
            </div>
        </div>
    </div>
</div>

    <script src="dist/js/jquery-3.4.1.min.js"></script>                    
    <script src="dist/js/umd/popper.min.js"></script>                   
    <script src="dist/js/bootstrap.min.js"></script>

</body>
</html>