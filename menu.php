<?php
echo '
<nav class="navbar navbar-default container" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        
    </div>
    <div class="collapse navbar-collapse navbar-responsive-collapse">
        <ul class="nav navbar-nav sm" data-smartmenus-id="15724499035408755">
            
            <li><a href="https://coder-forge.com/index.php">Home</a></li>
            <!--  <li class="dropdown"><a href="https://coder-forge.com/meetups.php">Meetups</a></li> -->
            <li class="dropdown"><a href="https://coder-forge.com/resources.php">Resources</a></li>
            <li class="dropdown"><a href="https://coder-forge.com/forgers.php">Forgers</a></li>
            <!-- <li class="dropdown"><a href="https://coder-forge.com/about.php">About</a></li> -->
        </ul>
    </div>
</nav>
';
?>