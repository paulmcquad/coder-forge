<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js wf-bree-n4-active wf-bree-n7-active wf-breeserif-n7-active wf-breeserif-n3-active wf-active"><!--<![endif]-->

<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Coder Forge - Home</title>
    <link rel="shortcut icon" type="image/x-icon" href="img/icon.png" />

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style type="text/css">.tk-bree{font-family:"bree",sans-serif;}.tk-bree-serif{font-family:"bree-serif",serif;}</style><style type="text/css">@font-face{font-family:bree;src:url(https://use.typekit.net/af/be7132/000000000000000000013389/27/l?subset_id=2&fvd=n4&v=3) format("woff2"),url(https://use.typekit.net/af/be7132/000000000000000000013389/27/d?subset_id=2&fvd=n4&v=3) format("woff"),url(https://use.typekit.net/af/be7132/000000000000000000013389/27/a?subset_id=2&fvd=n4&v=3) format("opentype");font-weight:400;font-style:normal;}@font-face{font-family:bree;src:url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/l?subset_id=2&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/d?subset_id=2&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/b5463b/00000000000000000001338b/27/a?subset_id=2&fvd=n7&v=3) format("opentype");font-weight:700;font-style:normal;}@font-face{font-family:bree-serif;src:url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/l?subset_id=2&fvd=n7&v=3) format("woff2"),url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/d?subset_id=2&fvd=n7&v=3) format("woff"),url(https://use.typekit.net/af/31d37f/00000000000000000001441b/27/a?subset_id=2&fvd=n7&v=3) format("opentype");font-weight:700;font-style:normal;}@font-face{font-family:bree-serif;src:url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/l?subset_id=2&fvd=n3&v=3) format("woff2"),url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/d?subset_id=2&fvd=n3&v=3) format("woff"),url(https://use.typekit.net/af/fc0d08/000000000000000000014418/27/a?subset_id=2&fvd=n3&v=3) format("opentype");font-weight:300;font-style:normal;}</style><script type="text/javascript">try{Typekit.load({async: false});}catch(e){}</script>

    <link rel="stylesheet" href="dist/css/9b21f50b6868.css" type="text/css">
    <link rel="stylesheet" href="dist/css/main.css" type="text/css">

</head>

<body class="template-home-page" cz-shortcut-listen="true">

    <section class="container">
        <header class="row" id="header">
            <!-- Begin grid -->
            <a href="https://coder-forge.com">
                <h1><img src="img/profile.jpg"
                     class="logo img-rounded" height="42" width="42"/>
                <span id="pyhead">Coder Forge</span></h1></a>
            <!-- End grid -->
        </header>
        <!-- header -->
    </section> <!-- container -->

    <div class="menu">
        <?php include 'menu.php';?>
    </div>

<div class="container">
    <div class="row well">
        <div class="col-md-8">
            <div class="row">
                <article>

                    <!-- <section id="resources"> -->

                        <h2>Resources:</h2>

                        <h3>HTML + CSS</h3>

                        <div class="rich-text">
                            <p>
                            Both HTML and CSS are widely used by developers to make websites! While HTML provides
                            the structure, CSS adds the styling to websites. Although not programming languages, every developer
                            benefits from knowing them!  
                            </p>

                            <div class="list-group">
                                    <a href="https://www.w3schools.com/html/default.asp" target="_blank" class="list-group-item">W3Schools
                                      Tutorial on HTML</a>
                                    <a href="https://www.w3schools.com/css/default.asp" target="_blank" class="list-group-item">W3Schools
                                      Tutorial on CSS</a>
                                    <a href="https://www.codecademy.com/learn/paths/learn-how-to-build-websites" target="_blank"
                                      class="list-group-item">Codecademy Course on Web Development with HTML + CSS</a>
                                  </div>
                        </div>

                        <h3>JavaScript</h3>

                        <div class="rich-text">
                            <p>
                             JavaScript is a programming language that has many applications, most commonly being
                             used to add interactivity and responsiveness to websites. A great programming language to learn and build
                             upon your HTML and CSS skills!
                            </p>

                            <div class="list-group">
                                    <a href="https://www.w3schools.com/js/default.asp" target="_blank" class="list-group-item">W3Schools
                                      Tutorial on JavaScript</a>
                                    <a href="https://javascript.info/" target="_blank" class="list-group-item">Javascript.info Tutorial</a>
                                    <a href="https://www.javatpoint.com/javascript-tutorial" target="_blank"
                                      class="list-group-item">Javatpoint Tutorial on JavaScript</a>
                                  </div>
                        </div>
                        
                        <h3>Python</h3>

                        <div class="rich-text">
                            <p>
                             Python is a general purpose, object-oriented programming language. It has many uses,
                             such as scripting, development of web applications, machine learning and security. Many people pick up
                             Python as their first language because of its easy to learn syntax.
                            </p>

                        <div class="list-group">
                                    <a href="https://docs.python.org/3/tutorial/index.html" target="_blank" class="list-group-item">Oficial
                                      Python.org Tutorial</a>
                                    <a href="https://www.sololearn.com/" target="_blank" class="list-group-item">SoloLearn Website</a>
                                    <a href="https://pythoninstitute.org/free-python-courses/?gclid=Cj0KCQjwkK_qBRD8ARIsAOteukBnZCea00nmlkyCA7oeF8kHFgmzVj1yYuRC6imRr2Pa7hMGMVB9WhEaAkkDEALw_wcB"
                                      target="_blank" class="list-group-item">Python Institute Tutorial</a>
                             </div>
                        </div>

                        <h3>Java</h3>

                        <div class="rich-text">
                            <p>
                             Java is a general purpose, object-oriented programming language. It has a <i>write
                             once, run anywhere</i> approach, where the code writen in one environment will run in any other provided
                             it has a Java Virtual Machine. Java is heavily used in client-server communication and it's also used for
                             the development of Android application.
                            </p>

                            <div class="list-group">
                                    <a href="https://docs.oracle.com/javase/tutorial/java/index.html" target="_blank"
                                      class="list-group-item">Oracle's oficial Java Tutorial</a>
                                    <a href="https://hi.hyperskill.org/projects3" target="_blank" class="list-group-item">JetBrains Academy
                                      Course</a>
                                    <a href="https://beginnersbook.com/java-tutorial-for-beginners-with-examples/" target="_blank"
                                      class="list-group-item">BeginnersBook Java Tutorial</a>
                            </div>
                        </div>

                        <h3>C++</h3>

                        <div class="rich-text">
                            <p>
                                C++ is a general-purpose programming language created by Bjarne Stroustrup as an extension of the C programming language,
                                or "C with Classes". The language has expanded significantly over time, and modern C++ has object-oriented, generic, 
                                functional features in addition to facilities for low-level memory manipulation.
                            </p>

                            <div class="list-group">
                                    <a href="https://www.geeksforgeeks.org/cpp-tutorial/" target="_blank"
                                      class="list-group-item">Geeksforgeeks C++ Tutorial</a>
                                    <a href="https://www.tutorialspoint.com/cplusplus/index.htm" target="_blank"
                                      class="list-group-item">Tutorialspoint C++ Tutorial</a>
                                    <a href="https://beginnersbook.com/2017/08/c-plus-plus-tutorial-for-beginners/" target="_blank"
                                      class="list-group-item">BeginnersBook C++ Tutorial</a>
                            </div>
                        </div>
                    <!-- </section> -->

                </article>
            </div>
        </div>
    </div>
</div>
                        
    <script src="dist/js/jquery-3.4.1.min.js"></script>                    
    <script src="dist/js/umd/popper.min.js"></script>                   
    <script src="dist/js/bootstrap.min.js"></script>

</body>
</html>